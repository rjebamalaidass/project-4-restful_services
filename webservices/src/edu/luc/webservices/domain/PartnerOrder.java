package edu.luc.webservices.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="WEBSERVICES_PARTNER_ORDER")
public class PartnerOrder {
	
	@Id
	@Column(name="ORDERID")
	private int orderid;
	
	@Column(name="PARTNERID")
	private int partnerid;
	
	@Column(name="ORDERFULLFILLMENTSTATUS")
	private String orderfullfillmentstatus;
	
	@Column(name="DATEFULLFILLED")
	private Date datefullfilled;
	
	@Column(name="DATEORDERRECEIVED")
	private Date dateorderreceived;
	
	@Column(name="QUANTITY")
	private int quantity;
	
	@Column(name="PRODUCTID")
	private int productid;
	
	@Column(name="PRODUCTNAME")
	private String productname;
	
	
	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	@OneToOne
	@JoinColumn(name="ORDERID")
	private Order order;
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@OneToOne
	@JoinColumn(name="PARTNERID", insertable = false, updatable = false)
	private Partner partner;


	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}

	public int getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(int partnerid) {
		this.partnerid = partnerid;
	}

	public String getOrderfullfillmentstatus() {
		return orderfullfillmentstatus;
	}

	public void setOrderfullfillmentstatus(String orderfullfillmentstatus) {
		this.orderfullfillmentstatus = orderfullfillmentstatus;
	}

	public Date getDatefullfilled() {
		return datefullfilled;
	}

	public void setDatefullfilled(Date datefullfilled) {
		this.datefullfilled = datefullfilled;
	}

	public Date getDateorderreceived() {
		return dateorderreceived;
	}

	public void setDateorderreceived(Date dateorderreceived) {
		this.dateorderreceived = dateorderreceived;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	@Override
	public String toString() {
		return "PartnerOrder [orderid=" + orderid + ", partnerid=" + partnerid + ", orderfullfillmentstatus="
				+ orderfullfillmentstatus + ", datefullfilled=" + datefullfilled + ", dateorderreceived="
				+ dateorderreceived + ", quantity=" + quantity + ", productid=" + productid + ", productname="
				+ productname + ", order=" + order + ", partner=" + partner + "]";
	}
	
	
	
	
	

}
