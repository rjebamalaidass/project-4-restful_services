package edu.luc.webservices.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name="WEBSERVICES_PARTNER")
public class Partner {
	
	@Id
	@SequenceGenerator(name = "SEQ_WEBSERVICES_PARTNER", sequenceName = "SEQ_WEBSERVICES_PARTNER", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WEBSERVICES_PARTNER")
	@Column(name="PARTNERID")
	private int partnerid;
	
	@Column(name="NAME")
	private String partnername;
	
	@Column(name="EMAILADDRESS")
	private String emailaddress;
	
	@Column(name="PHONE")
	private String phone;
	
	@Column(name="ADDRESSID")
	private int addressid;
	
	@OneToOne
	@JoinColumn(name="ADDRESSID", insertable = false, updatable = false)
	private Address address;

	public int getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(int partnerid) {
		this.partnerid = partnerid;
	}

	public String getPartnername() {
		return partnername;
	}

	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}

	public String getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getAddressid() {
		return addressid;
	}

	public void setAddressid(int addressid) {
		this.addressid = addressid;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Partner [partnerid=" + partnerid + ", partnername=" + partnername + ", emailaddress=" + emailaddress
				+ ", phone=" + phone + ", addressid=" + addressid + ", address=" + address.toString() + "]";
	}
	
	

}
