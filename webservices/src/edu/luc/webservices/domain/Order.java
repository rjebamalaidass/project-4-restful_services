package edu.luc.webservices.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WEBSERVICES_ORDER")
public class Order {
	
	
	@Id
	@SequenceGenerator(name = "SEQ_WEBSERVICES_ORDER", sequenceName = "SEQ_WEBSERVICES_ORDER", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WEBSERVICES_ORDER")
	@Column(name="ORDERID")
	private int orderid;
	
	
	@Column(name="USERID")
	private int userid;
	
	
	@Column(name="PAYMENTSTATUS")
	private String paymentstatus;
	
	@Column(name="PAYMENTCONFIRMATION")
	private String paymentconfirmation;
	
	@Column(name="ORDERSTATUS")
	private String orderstatus;
	
	@Column(name="ORDERPRICE")
	private double orderprice;
	
	@Column(name="ADDRESSID")
	private int addressid;
	
	@Column(name="SHIPPINGCOST")
	private double shippingcost;
	
	@Column(name="TOTALPRICE")
	private double totalprice;
	
	@Column(name="ORDERDATE")
	private Date orderdate;
	
	@Column(name="SHIPPINGDATE")
	private Date shippingdate;
	
	@Column(name="DATERECEIVED")
	private Date datereceived;
	
	
	@Column(name="SHIPPINGSTATUS")
	private String shippingstatus;
	
	@Column(name="ACTIVE")
	private String active;
	
	@Column(name="REFUND_STATUS")
	private String refundstatus;
	
	@Column(name="CANCELLED_DATE")
	private Date cancelledDate;
	
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getRefundstatus() {
		return refundstatus;
	}

	public void setRefundstatus(String refundstatus) {
		this.refundstatus = refundstatus;
	}

	public Date getCancelledDate() {
		return cancelledDate;
	}

	public void setCancelledDate(Date cancelledDate) {
		this.cancelledDate = cancelledDate;
	}

	@OneToOne
	@JoinColumn(name="USERID", insertable = false, updatable = false)
	private User user;
	
	@OneToOne
	@JoinColumn(name="ADDRESSID", insertable = false, updatable = false)
	private Address address;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int getOrderid() {
		return orderid;
	}

	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}



	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPaymentstatus() {
		return paymentstatus;
	}

	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}

	public String getPaymentconfirmation() {
		return paymentconfirmation;
	}

	public void setPaymentconfirmation(String paymentconfirmation) {
		this.paymentconfirmation = paymentconfirmation;
	}

	public String getOrderstatus() {
		return orderstatus;
	}

	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}

	public double getOrderprice() {
		return orderprice;
	}

	public void setOrderprice(double orderprice) {
		this.orderprice = orderprice;
	}

	public int getAddressid() {
		return addressid;
	}

	public void setAddressid(int addressid) {
		this.addressid = addressid;
	}

	public double getShippingcost() {
		return shippingcost;
	}

	public void setShippingcost(double shippingcost) {
		this.shippingcost = shippingcost;
	}

	public double getTotalprice() {
		return totalprice;
	}

	public void setTotalprice(double totalprice) {
		this.totalprice = totalprice;
	}

	public Date getOrderdate() {
		return orderdate;
	}

	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}

	public Date getShippingdate() {
		return shippingdate;
	}

	public void setShippingdate(Date shippingdate) {
		this.shippingdate = shippingdate;
	}

	public Date getDatereceived() {
		return datereceived;
	}

	public void setDatereceived(Date datereceived) {
		this.datereceived = datereceived;
	}

	public String getShippingstatus() {
		return shippingstatus;
	}

	public void setShippingstatus(String shippingstatus2) {
		this.shippingstatus = shippingstatus2;
	}

	@Override
	public String toString() {
		return "Order [orderid=" + orderid + ", userid=" + userid + ", paymentstatus=" + paymentstatus
				+ ", paymentconfirmation=" + paymentconfirmation + ", orderstatus=" + orderstatus + ", orderprice="
				+ orderprice + ", addressid=" + addressid + ", shippingcost=" + shippingcost + ", totalprice="
				+ totalprice + ", orderdate=" + orderdate + ", shippingdate=" + shippingdate + ", datereceived="
				+ datereceived + ", shippingstatus=" + shippingstatus + ", active=" + active + ", refundstatus="
				+ refundstatus + ", cancelledDate=" + cancelledDate + ", user=" + user + ", address=" + address + "]";
	}


	
	
	

}
