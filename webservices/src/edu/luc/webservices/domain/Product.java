package edu.luc.webservices.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WEBSERVICES_PRODUCT")
public class Product {
	
	@Id
	@SequenceGenerator(name = "SEQ_WEBSERVICES_PRODUCT", sequenceName = "SEQ_WEBSERVICES_PRODUCT", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_WEBSERVICES_PRODUCT")
	@Column(name="PRODUCTID")
	private int productid;
	
	@Column(name="PRODUCTNAME")
	private String productname;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="CATEGORY")
	private String category;
	
	@Column(name="PRICE")
	private double price;
	
	@Column(name="PARTNERID")
	private int partnerid;
	
	@Column(name="AVAILABLE_QUANTITY")
	private int availablequantity;
	
	@OneToOne
	@JoinColumn(name="PARTNERID" , insertable = false, updatable = false)
	private Partner partner;

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(int partnerid) {
		this.partnerid = partnerid;
	}

	public int getAvailablequantity() {
		return availablequantity;
	}

	public void setAvailablequantity(int availablequantity) {
		this.availablequantity = availablequantity;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	@Override
	public String toString() {
		return "Product [productid=" + productid + ", productname=" + productname + ", description=" + description
				+ ", category=" + category + ", price=" + price + ", partnerid=" + partnerid + ", availablequantity="
				+ availablequantity + ", partner=" + partner.toString() + "]";
	}
	
	
	
	
	

}
