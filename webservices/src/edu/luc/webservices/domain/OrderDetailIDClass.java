package edu.luc.webservices.domain;

import java.io.Serializable;

public class OrderDetailIDClass implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2777220660441289734L;


	private int orderid;

	
	private int productid;
	
	public OrderDetailIDClass(int orderid, int productid) {
		super();
		this.orderid = orderid;
		this.productid = productid;
	}
	
	

	public OrderDetailIDClass() {
		super();
	}



	public int getOrderid() {
		return orderid;
	}



	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}



	public int getProductid() {
		return productid;
	}



	public void setProductid(int productid) {
		this.productid = productid;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + orderid;
		result = prime * result + productid;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderDetailIDClass other = (OrderDetailIDClass) obj;
		if (orderid != other.orderid)
			return false;
		if (productid != other.productid)
			return false;
		return true;
	}
	
	




}
