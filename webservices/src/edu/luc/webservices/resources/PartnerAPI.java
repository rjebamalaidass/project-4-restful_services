package edu.luc.webservices.resources;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import edu.luc.webservices.bean.OrderOfPartner;


@Path("/partner")
@WebService
public interface PartnerAPI {
	
    @GET
    @Path("order/{partnerid}")
    @Produces(MediaType.APPLICATION_JSON)
    List<OrderOfPartner> getOrderList(@PathParam("partnerid") int partnerid) throws Exception;
    
    @POST
    @Path("order/{partnerid}/{orderid}")
    @Produces(MediaType.APPLICATION_JSON)
    OrderOfPartner updateOrderstatus(@PathParam("partnerid") int partnerid, @PathParam("orderid") int orderid, OrderOfPartner orderOfPartner);

}
