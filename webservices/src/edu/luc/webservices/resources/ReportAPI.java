package edu.luc.webservices.resources;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import edu.luc.webservices.bean.OrderOfPartner;
import edu.luc.webservices.bean.WebOrder;

@Path("/report")
@WebService
public interface ReportAPI {
	
  @GET
  @Path("orderdetails/{userid}/status")
  @Produces(MediaType.APPLICATION_JSON)
  List<WebOrder> getAllOrderOfUser(@PathParam("userid") String userid);
  
  @GET
  @Path("orderdetails/{userid}/{status}")
  @Produces(MediaType.APPLICATION_JSON)
  List<WebOrder> getAllOrderOfUserByStatus(@PathParam("userid") int userid, @PathParam("status") String status);
  
  @GET
  @Path("orderdetails/{partnerid}")
  @Produces(MediaType.APPLICATION_JSON)
  List<OrderOfPartner> getAllOrderOfPartner(@PathParam("partnerid") int partnerid);
  
  @GET
  @Path("orderdetails/{partnerid}/{status}")
  @Produces(MediaType.APPLICATION_JSON)
  List<OrderOfPartner> getPartnerOrderByStatus(@PathParam("partnerid") int partnerid, @PathParam("status") String status);
  
  

}
