package edu.luc.webservices.resources;

import javax.jws.WebService;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ShippingStatus;
import edu.luc.webservices.bean.WebAddress;
import edu.luc.webservices.bean.WebOrder;

@Path("/order")
@WebService
public interface OrderAPI {
	
    @POST
    @Path("{productid}/{userid}")
    @Produces(MediaType.APPLICATION_JSON)
    void placeOrder(@PathParam("productid") int productid, @PathParam("userid") int userid, WebOrder order) throws Exception;
    
    @POST
    @Path("address/{addressid}")
    @Produces(MediaType.APPLICATION_JSON)
    void updateAddress(@PathParam("addressid") int addressid, WebAddress address) throws Exception;
    

	@DELETE
	@Path("{orderid}")
	@Produces(MediaType.APPLICATION_JSON)
	WebOrder cancelOrder(@PathParam("orderid") int orderid);
	
	@POST
	@Path("orderdetail/{orderid}/{productid}")
	@Produces(MediaType.APPLICATION_JSON)
	WebOrder updateOrderQuantity(@PathParam("orderid") int orderid, @PathParam("productid") int productid, int quantity) throws Exception;
	    
    
    @GET
    @Path("orderDetail/{orderid}")
    @Produces(MediaType.APPLICATION_JSON)
	WebOrder getOrder(@PathParam("orderid") int orderid);
    
	@DELETE
	@Path("cart/{orderid}/{productid}")
	@Produces(MediaType.APPLICATION_JSON)
	WebOrder removeProductFromCart(@PathParam("orderid") int orderid, @PathParam("productid") int productid) throws Exception;
  

	
    @POST
    @Path("payment/{orderid}")
    @Produces(MediaType.APPLICATION_JSON)
    WebOrder paymentProcessing(@PathParam("orderid") int orderid, Payment payment) throws Exception;
    
    
 
    
    @POST
    @Path("shipping/{orderid}/")
    @Produces(MediaType.APPLICATION_JSON)
    WebOrder updateOrderShipping(@PathParam("orderid") int orderid, ShippingStatus status) throws Exception;
  
    



}
