package edu.luc.webservices.resources;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Service;


import edu.luc.webservices.bean.WebOrder;
import edu.luc.webservices.bean.Login;
import edu.luc.webservices.bean.OrderOfPartner;
import edu.luc.webservices.bean.Payment;
import edu.luc.webservices.bean.ProductBean;
import edu.luc.webservices.bean.RegisterDetail;
import edu.luc.webservices.bean.WebAddress;


@Path("/product")
@WebService
public interface ProductAPI {

      
    
    @GET
    @Path("/productlist")
    @Produces(MediaType.APPLICATION_JSON)
    List<ProductBean> getProductList();
    
    
    @GET
    @Path("productdetails/{productid}")
    @Produces(MediaType.APPLICATION_JSON)
    ProductBean getProductDetails(@PathParam("productid") int productid);
    
    
    
    @GET
    @Path("category")
    @Produces(MediaType.APPLICATION_JSON)
    List<String> getCategories();
    
    @GET
    @Path("category/{category}")
    @Produces(MediaType.APPLICATION_JSON)
    List<ProductBean> getProductByCategory(@PathParam("category") String a);
    

    
    @POST
    @Path("product/{partnerid}")
    @Produces(MediaType.APPLICATION_JSON)
    ProductBean addPartnerProduct(@PathParam("partnerid") int partnerid, ProductBean product);
    
    @POST
    @Path("product")
    @Produces(MediaType.APPLICATION_JSON)
    ProductBean updateProductDetail(ProductBean product);
    




}