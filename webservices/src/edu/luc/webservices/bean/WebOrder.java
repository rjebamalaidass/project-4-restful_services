package edu.luc.webservices.bean;



import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebOrder  extends AbstractRepresentation {
	
	@JsonProperty("orderId")
	public int orderId;
	
	
	@JsonProperty("productList")
	public List<ProductOrder> productList;
		
	@JsonProperty("address")
	public WebAddress address;
		
	@JsonProperty("orderstatus")
	private String orderstatus;
	
	@JsonProperty("orderprice")
	private double orderprice;
	
	@JsonProperty("shippingcost")
	private double shippingcost;
	
	@JsonProperty("totalprice")
	private double totalprice;
	
	@JsonProperty("orderdate")
	private Date orderdate;
	
	
	
	
	
	
	
	
	
	public String getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	public double getOrderprice() {
		return orderprice;
	}
	public void setOrderprice(double orderprice) {
		this.orderprice = orderprice;
	}
	public double getShippingcost() {
		return shippingcost;
	}
	public void setShippingcost(double shippingcost) {
		this.shippingcost = shippingcost;
	}
	public double getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(double totalprice) {
		this.totalprice = totalprice;
	}
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public WebAddress getAddress() {
		return address;
	}
	public void setAddress(WebAddress address) {
		this.address = address;
	}
	public List<ProductOrder> getProductList() {
		return productList;
	}
	public void setProductList(List<ProductOrder> productList) {
		this.productList = productList;
	}
	@Override
	public String toString() {
		return "WebOrder [orderId=" + orderId + ", productList=" + productList + ", address=" + address
				+ ", orderstatus=" + orderstatus + ", orderprice=" + orderprice + ", shippingcost=" + shippingcost
				+ ", totalprice=" + totalprice + ", orderdate=" + orderdate + "]";
	}

	
	
	
	
	

}
