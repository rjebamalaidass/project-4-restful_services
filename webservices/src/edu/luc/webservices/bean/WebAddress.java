package edu.luc.webservices.bean;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebAddress {
	
	@JsonProperty("addressid")
	private int addressid;
	
	@JsonProperty("addresstype")
	private String addresstype;
	
	@JsonProperty("userid")
	private int userid;
	
	
	@JsonProperty("address1")
	private String address1;
	
	
	@JsonProperty("address2")
	private String address2;
	
	@JsonProperty("city")
	private String city;
	
	@JsonProperty("state")
	private String state;
	
	@JsonProperty("zip")
	private String zip;

	public int getAddressid() {
		return addressid;
	}

	public void setAddressid(int addressid) {
		this.addressid = addressid;
	}

	public String getAddresstype() {
		return addresstype;
	}

	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Override
	public String toString() {
		return "WebAddress [addressid=" + addressid + ", addresstype=" + addresstype + ", userid=" + userid
				+ ", address1=" + address1 + ", address2=" + address2 + ", city=" + city + ", state=" + state + ", zip="
				+ zip + "]";
	}
	
	

}
