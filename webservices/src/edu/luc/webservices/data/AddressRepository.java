package edu.luc.webservices.data;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.luc.webservices.domain.Address;

@Configuration
public interface AddressRepository extends JpaRepository<Address, Integer> {

}
