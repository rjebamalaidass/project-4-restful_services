package edu.luc.webservices.data;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.luc.webservices.domain.Review;

public interface ReviewRepository extends JpaRepository<Review, Integer> {

}
