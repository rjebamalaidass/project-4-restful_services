package edu.luc.webservices.data;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.luc.webservices.domain.Login;

public interface LoginRepository extends JpaRepository<Login, String>{
	public Login findByUsername(String username);
}
