package edu.luc.webservices.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.data.AddressRepository;
import edu.luc.webservices.domain.Address;
import edu.luc.webservices.domain.Order;

@Service
public class AddressService {
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	OrderService orderService;
	
	public int insertAddress(int customerid, String type, String address1, String address2, String city, String state, String zip)
	{
		Address address = new Address();
		address.setAddresstype(type);
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		address.setUserid(customerid);
		
		address = addressRepository.saveAndFlush(address);
		return address.getAddressid();
	}
	
	public void updateAddressType(int addressid, String type)
	{
		Address address = addressRepository.getOne(addressid);
		address.setAddresstype(type);
		
		addressRepository.saveAndFlush(address);
	}
	
	public void updateAddress(int addressid, String address1, String address2, String city, String state, String zip)
	{
		Address address = addressRepository.getOne(addressid);
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		addressRepository.saveAndFlush(address);
	}
	
	public void removeAddress(int addressid)
	{
		addressRepository.deleteById(addressid);
	}

	public Boolean updateShippingAddress(int orderid, Address address) {
		
		Order order = orderService.getOrderDetails(Integer.valueOf(orderid));
		
		int userid = order.getUserid();
		int addressid = insertAddress(userid, "SHIPPING", address.getAddress1(), address.getAddress2(), 
				address.getCity(), address.getState(), address.getZip());
		order.setAddressid(addressid);
		orderService.saveOrder(order);
		return true;
		
	}
	
	

}
