package edu.luc.webservices.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.bean.ProductBean;
import edu.luc.webservices.data.ProductRepository;
import edu.luc.webservices.domain.Product;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	public Product getProductById(Integer productId)
	{
		return productRepository.findById(productId).get();
	}
	
	public List<Product> getProductByCategory(String category)
	{
		return productRepository.findByCategory(category);
	}
	
	public List<Product> getProductByName(String name)
	{
		return productRepository.findByProductnameContains(name);
	}
	
	public List<String> getAllProductCategories()
	{
		return productRepository.findDistinctCategory();
	}
	
	public List<Product> getAllProducts() {
		List<Product> prodList = productRepository.findAll();
		return prodList;
		
		
	}
	
	
	public int insertProduct(String name, String description, String category, int partnerid)
	{
		Product product = new Product();
		product.setProductname(name);
		product.setDescription(description);
		product.setCategory(category);
		product.setPartnerid(partnerid);
		product.setAvailablequantity(150);
		product.setPrice(50.00);
		
		product = productRepository.saveAndFlush(product);
		return product.getProductid();
		
		
	}
	
	public void updatePrice(int productid, double price)
	{
		Product product = productRepository.getOne(productid);
		product.setPrice(price);
		productRepository.saveAndFlush(product);
	}
	
	public void updateName(int productid, String name)
	{
		Product product = productRepository.getOne(productid);
		product.setProductname(name);
		productRepository.saveAndFlush(product);
	}
	
	public void updateDescription(int productid, String description)
	{
		Product product = productRepository.getOne(productid);
		product.setDescription(description);
		productRepository.saveAndFlush(product);
	}
	
	public void updateQuantity(int productid, int quantity)
	{
		Product product = productRepository.getOne(productid);
		product.setAvailablequantity(quantity);
		productRepository.saveAndFlush(product);
	}

	public void updateProductDetail(ProductBean product) {
		Product p = productRepository.getOne(product.getProductid());
		p.setProductname(product.getProductname());
		p.setDescription(product.getDescription());
		p.setCategory(product.getCategory());
		p.setAvailablequantity(product.getAvailablequantity());
		p.setPrice(product.getPrice());
		productRepository.saveAndFlush(p);
		
	}

	
	
	}


