package edu.luc.webservices.services;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.bean.RegisterDetail;
import edu.luc.webservices.data.AddressRepository;
import edu.luc.webservices.data.UserRepository;
import edu.luc.webservices.domain.Address;
import edu.luc.webservices.domain.User;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired 
	AddressRepository addressRepository;
	
	
	/**
	 * Customer and Partner details are stored in the same table WEBSERVICES_USER. Each user is differentiated by the Type column value.
	 * If the value is "C", the the user is considered as Customer
	 * If the value is "P" the user is considered as Partner
	 */
	public User registerUser(RegisterDetail register)
	{
		User user =  InsertUser(register.getFirstName()+" "+register.getLastName(), register.getPhone(), register.getEmail(), register.getType(),
				register.getAddress1(), register.getAddress2(), register.getCity(), register.getState(), register.getZip(), register.getAddressType());
		
		Optional<User> finalUser =  userRepository.findById(user.getUserid());
		if(finalUser.isPresent())
		{
			return finalUser.get();
		}
		else
		{
			return null;
		}
		
	}
	
	public User InsertUser(String name, String phone, String email, String type, String address1, String address2, String city, String state, String zip, String addresstype)
	{
		User user = new User();
		user.setName(name);
		user.setPhone(phone);
		user.setEmail(email);
		user.setType(type);
		user = userRepository.saveAndFlush(user);
		
		
		Address address = new Address();
		address.setAddresstype(addresstype);
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		address.setUserid(user.getUserid());
		
		addressRepository.saveAndFlush(address);
		
		
		return findUser(user.getUserid());
		
	}
	
	public User findUser(int userid)
	{
		Optional<User> userOptional =  userRepository.findById(userid);
		User user = userOptional.get();
		//System.out.println(user.toString());
		return user;
	}
	
	
	

}
