package edu.luc.webservices.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.data.LoginRepository;
import edu.luc.webservices.domain.Login;
import edu.luc.webservices.domain.User;

@Service
public class LoginService {
	
	@Autowired
	LoginRepository loginRepository;
	
	@Autowired
	UserService userService;
	
	public User setupLoginCredentials(int customerId, String  username, String password ) throws Exception
	{
		Login login = new Login();
		login.setCustomerid(customerId);
		login.setPassword(password);
		login.setUsername(username);
		loginRepository.save(login);
		
		return userService.findUser(customerId);
		
	}
	
	public void updatePassword(String username, String password)
	{
		Login login = loginRepository.findByUsername(username);
		login.setPassword(password);
		loginRepository.saveAndFlush(login);
	}
	
	public Login getPasswordForCompare(String username)
	{
		return loginRepository.findByUsername(username);
	}
	
	/***
	 * After authentication, we pass the whole user object to the front end
	 * based on the type column value, the frontend will determine the user type and show corresponding values.
	 * If the type value is "C" then the user is a customer
	 * if the type value is "P", then the user is a partner
	 *
	 */
	
	public User authenticate(String username, String password)
	{
		Login login = getPasswordForCompare(username);
		
		if(StringUtils.equals(login.getPassword(), password))
		{
			return userService.findUser(login.getCustomerid());
		}
		else
		{
			return null;
		}
	}

}
