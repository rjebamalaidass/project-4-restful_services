package edu.luc.webservices.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.luc.webservices.data.PartnerOrderRepository;
import edu.luc.webservices.domain.Order;
import edu.luc.webservices.domain.PartnerOrder;

@Service
public class PartnerOrderService {
	
	@Autowired
	PartnerOrderRepository partnerOrderRepository;
	
	public void insertPartnerOrder(int partnerid, int orderid, Date orderDate, int quantity, int productid, String productname)
	{
		PartnerOrder order = new PartnerOrder();
		order.setPartnerid(partnerid);
		order.setOrderid(orderid);
		order.setDateorderreceived(orderDate);
		order.setQuantity(quantity);
		order.setProductid(productid);
		order.setProductname(productname);
		partnerOrderRepository.saveAndFlush(order);
		
	}
	
	public void updateOrderStatus(int orderid, String orderStatus)
	{
		PartnerOrder order = partnerOrderRepository.getOne(orderid);
		order.setOrderfullfillmentstatus(orderStatus);
		partnerOrderRepository.saveAndFlush(order);
	}
	
	public PartnerOrder updateOrderStatusAndDate(int orderid, String orderStatus, Date dateFullfilled)
	{
		PartnerOrder order = partnerOrderRepository.getOne(orderid);
		order.setOrderfullfillmentstatus(orderStatus);
		order.setDatefullfilled(dateFullfilled);
		return partnerOrderRepository.saveAndFlush(order);
	}
	
	public void updateDateFulfilled(int orderid, Date dateFullfilled)
	{
		PartnerOrder order = partnerOrderRepository.getOne(orderid);
		order.setDatefullfilled(dateFullfilled);
		partnerOrderRepository.saveAndFlush(order);
	}
	
	public List<PartnerOrder> getPartnerOrders(int partnerid) {
		return partnerOrderRepository.findByPartnerid(partnerid);
	}

}
